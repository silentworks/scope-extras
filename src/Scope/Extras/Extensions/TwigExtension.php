<?php

namespace Scope\Extras\Extensions;

use Scope\Extras\Datasource\Scope;

class TwigExtension extends \Twig_Extension
{
    public function __construct($config)
    {
        $this->config = $config;
    }

    public function getName()
    {
        return 'Scope';
    }

    public function getGlobals()
    {
        return array(
            'scope' => Scope::getNodes($this->config)
        );
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('makeUrl', array($this, 'makeUrl')),
            new \Twig_SimpleFunction('themeUrl', array($this, 'themeUrl')),
        );
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('url', array($this, 'makeUrl')),
            new \Twig_SimpleFilter('themeUrl', array($this, 'themeUrl')),
        );
    }

    public function themeUrl()
    {
        return empty($currentPath) ? $this->config['currentThemePath'] :
                        $this->config['currentThemePath'] . $currentPath;
    }

    public function makeUrl($id, $context = null, $args = null, $scheme = -1, array $options = array())
    {
        $modx = $this->config['modx'];
        return $modx->makeUrl($id, $context, $args, $scheme, $options);
    }
}
