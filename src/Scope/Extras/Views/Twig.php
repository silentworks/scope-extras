<?php

namespace Scope\Extras\Views;

class Twig extends \Scope\Core\Simple
{
	public function getEnvironment()
    {
        if (!isset($this->twigEnv) || !$this->twigEnv) {
        	$this->themeDir = sprintf("%s/%s/templates", $this->getThemesDirectory(), $this->getTheme());

        	\Twig_Autoloader::register();
            $fileBased = new \Twig_Loader_Filesystem($this->themeDir);
            $stringBased = new \Twig_Loader_String();

            $loader = new \Twig_Loader_Chain(array($fileBased, $stringBased));
            $this->twigEnv = new \Twig_Environment($loader, array(
                //'cache' => sprintf('%s/thin/%s', $this->getConfig('cachePath'), $this->getConfig('currentContext')),
            ));

            // Load Default
            $this->twigEnv->addExtension(new \Scope\Extras\Extensions\TwigExtension($this->getConfig()));

            foreach ($this->plugins as $ext) {
                $extension = is_object($ext) ? $ext : new $ext($this->getConfig());
                $this->twigEnv->addExtension($extension);
            }
        }
        return $this->twigEnv;
    }

	public function render($fileContent)
	{
		$env = $this->getEnvironment();

		return $env->render($fileContent, $this->data);
	}
}
