<?php

namespace Scope\Extras\Datasource\Xpdo;

class Resources
{
	/**
     * @var xPDO
     */
    public $xpdo;

	public function __construct(\modX $modx)
	{
		$this->xpdo = $modx;
	}

	public function get($parent = null, $showHidden = false, $showDeleted = false)
    {
        $c = $this->xpdo->newQuery('modResource');
        if ($parent || $parent === 0) {
            $c->where(array('parent' => $parent));
        }

        $c->andCondition(array(
            'deleted' => $showDeleted,
            'hidemenu' => $showHidden,
        ));

        $pages = $this->xpdo->getCollectionGraph('modResource', '{ "TemplateVarResources": {} }', $c);

        $pageList = array();
        foreach ($pages as $page) {
            $pageArray = $page->toArray();
            foreach($page->getMany('TemplateVars') as $tv) {
                $val = $tv->getValue($page->get('id'));
                $tvArray = $tv->toArray();

                if (isset($tvArray['output_properties']['delimiter'])) {
                    $val = explode($tvArray['output_properties']['delimiter'], $val);
                }

                $pageArray['tv'][$tv->get('name')] = $val;
            }
            $pageList[] = $pageArray;
        }

        return $pageList;
    }
}
