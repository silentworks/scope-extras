<?php

use \Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $table = 'site_templates';
}
