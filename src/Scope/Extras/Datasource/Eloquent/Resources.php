<?php

namespace Scope\Extras\Datasource\Eloquent;

class Resources
{
    public $db;

	public function __construct(\modX $modx)
	{
		$config = $modx->config;

        // Make a new connection
        $this->db = \Capsule\Database\Connection::make('default',array(
            'driver'    => $config['dbtype'],
            'host'      => $config['host'],
            'port'      => 3306,
            'database'  => $config['dbname'],
            'username'  => $config['username'],
            'password'  => $config['password'],
            'prefix'    => $config['table_prefix'],
            'charset'   => $config['charset'],
            'collation' => "utf8_general_ci",
        ), true);
	}

	public function get($parent = null, $showHidden = false, $showDeleted = false)
    {
        $pages = \Resource::all();
        // die(var_dump($pages));
        // $c = $this->xpdo->newQuery('modResource');
        // if ($parent || $parent === 0) {
        //     $c->where(array('parent' => $parent));
        // }

        // $c->andCondition(array(
        //     'deleted' => $showDeleted,
        //     'hidemenu' => $showHidden,
        // ));

        // $pages = $this->xpdo->getCollectionGraph('modResource', '{ "TemplateVarResources": {} }', $c);

        $pageList = array();
        foreach ($pages as $page) {
            // foreach($page->getMany('TemplateVars') as $tv) {
            //     $pageArray['tv'] = array(
            //         'caption' => $tv->get('caption'),
            //         $tv->get('name') => $tv->getValue($page->get('id'))
            //     );
            // }
            $pageList[] = $page;
        }

        return $pageList;
    }
}
