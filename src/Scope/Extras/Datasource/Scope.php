<?php
namespace Scope\Extras\Datasource;

class Scope
{
    protected $nodes = array();
	protected $protected;

	public function factory()
	{
		return new self();
	}

	public function __construct($config = null)
    {
        $this->config = $config;

        // Default Namespace
        $this->setNamespace();

        $resources = $this->getNamespace() . '\\Resources';

    	$this->setNode(array(
    		'pages' => new $resources($config['modx']),
    	));
    }

    public static function getNodes($config)
    {
    	$thn = new self($config);
    	return $thn->nodes;
    }

    public function setNode($node, $value = null)
    {
    	if (! is_array($node)) {
    		$node = array($node => $value);
    	}
    	$this->nodes = array_merge($this->nodes, $node);
    }

    public function setNamespace()
    {
        $this->namespace = '\\Scope\\Extras\\Datasource\\' . $this->config['datasource'];
    }

    public function getNamespace()
    {
        return $this->namespace;
    }
}
